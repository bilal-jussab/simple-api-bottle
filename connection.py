"""
Database Connection
Author: Bilal Jussab
"""

#######################################
# IMPORTS
#######################################
import sqlalchemy


#######################################
# MAIN
#######################################
connection_string = 'mysql+pymysql://local:local@localhost/testdb'


class Connection:

    def __init__(self):
        self.engine = sqlalchemy.create_engine(connection_string)

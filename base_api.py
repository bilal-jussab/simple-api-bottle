"""
Base API methods
Author: Bilal Jussab
"""


#######################################
# MAIN
#######################################
class BaseAPIMethods():

    def return_error(message=None):
        return {'message': message, 'success': False}

    def return_success(value=None):
        return {'data': value, 'success': True}

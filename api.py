"""
API Test - Python Bottle Framework
Author: Bilal Jussab
"""

#######################################
# IMPORTS
#######################################
import json
import logging
import bottle

from bottle import route

import functions
from base_api import BaseAPIMethods

app = application = bottle.default_app()


#######################################
# MAIN
#######################################
@route('/table', method=['GET'])
def get_table_data():
    """
    Returns data from the table table
    :return
        Successful responses have a data key and a success
        key with a value of true
        Unsuccessful responses have a message key and a success
        key with a value of false
    """
    try:
        logging.info("Getting table Data")
        return json.dumps(functions.get_table_data())
    except Exception as e:
        error_msg = "An error occurred getting table data"
        logging.error(error_msg)
        return BaseAPIMethods.return_error(message=error_msg)


@route('/table-two', method=['GET'])
def get_table_two_data():
    """
    Returns data from the table two table
    :return
        Successful responses have a data key and a success
        key with a value of true
        Unsuccessful responses have a message key and a success
        key with a value of false
    """
    try:
        logging.info("Getting table two Data")
        return json.dumps(functions.get_table_two_data())
    except Exception as e:
        error_msg = "An error occurred getting table two data"
        logging.error(error_msg)
        return BaseAPIMethods.return_error(message=error_msg)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)

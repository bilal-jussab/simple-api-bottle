"""
SQL Functions
Author: Bilal Jussab
"""

#######################################
# IMPORTS
#######################################
import connection
from base_api import BaseAPIMethods

conn = connection.Connection()


#######################################
# MAIN
#######################################
def get_table_data():
    """
    This function retrieves all the table data from the MS SQL Server DB.
    :return: Dict containing all table data.
    """
    query = "SELECT * FROM tableTest"
    data = conn.engine.execute(query).fetchall()
    result = [dict(row) for row in data]
    return BaseAPIMethods.return_success(value=result)


def get_table_two_data():
    """
    This function retrieves all the table two data from the MS SQL Server DB.
    :return: Dict containing all table two data.
    """
    query = "SELECT * FROM tableTwoTest"
    data = conn.engine.execute(query).fetchall()
    result = [dict(row) for row in data]
    return BaseAPIMethods.return_success(value=result)
